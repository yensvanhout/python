import subprocess

invoer = input("Geef een domein of IP adres op dat u wilt bereiken.:\n")

gereed = subprocess.run("ping -n 2 {}".format(invoer), capture_output=True,text=True,shell=True)

print("\nReturn code: ", gereed.returncode, "\n")
print("Uitvoer: ", gereed.stdout, "\n")
print("Errors: ",gereed.stderr)