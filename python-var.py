seconden_per_minute = 60
minuten_per_uur = 60
uren_per_dag = 24
dagen_per_week = 7

seconden_in_een_week = (((seconden_per_minute * minuten_per_uur) * uren_per_dag) * dagen_per_week)

print("Er zijn zo veel seconden in een week:", str(seconden_in_een_week))