byte1 = 11
byte2 = 25
byte3 = 60
byte4 = 55

if (byte1 >= 0 and byte1 < 255) and (byte2 >= 0 and byte2 < 255) and (byte3 >= 0 and byte3 < 255) and (byte4 >= 0 and byte4 < 255):
    if byte1 == 10:
        print("De vier bytes vormen een IPv4-adres in het bereik \"10.0.0.0/8\".")
    else:
        print("De vier bytes vormen een IPv4-adres buiten het bereik \"10.0.0.0/8\".")
else:
    print("De vier bytes vormen geen geldig IPv4-adres.")



if(byte1 > 255) and (byte2 > 255) and (byte3 > 255) and (byte4 > 255):
    print("De vier bytes vormen geen geldig IPv4-adres.")
else:
    if byte1 == 10:
        print("De vier bytes vormen een IPv4-adres in het bereik \"10.0.0.0/8\".")
    else:
        print("De vier bytes vormen een IPv4-adres buiten het bereik \"10.0.0.0/8\".")
