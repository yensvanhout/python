number_of_host_addresses = int( input("How many host addresses do you need?: ") )

bits = 2

while( pow( 2, bits ) - 2 ) < number_of_host_addresses:
    bits = bits + 1
    if bits >= 32:
        break

if bits >= 32:
    print( "Error too many hosts." )
else:
    print("You will need {} bits for the given amount of addresses.".format(bits))